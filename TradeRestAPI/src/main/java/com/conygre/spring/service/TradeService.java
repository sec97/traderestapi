package com.conygre.spring.service;

import com.conygre.spring.entities.Trade;
import com.conygre.spring.repo.TradeRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
//import java.util.Optional;
import java.util.Optional;

@Service
public class TradeService {

    @Autowired
    private TradeRepository dao;

    public void addTrade(Trade t) {
        dao.insert(t);

    }

    public Collection<Trade> getTrades() {
        return dao.findAll();
    }

    
    public Optional<Trade> getTradeById(ObjectId id) {
        return dao.findById(id);
    }

	public void deleteTradeById(ObjectId id) {
        dao.deleteById(id);
    }
    
    public void updateTradeById(Trade t) {
        dao.save(t);
    }

    public List<Trade> getTradeByStatus(String status)
    {
        return dao.findByTradeStatus(status);
    }
    
}