package com.conygre.spring.repo;

import java.util.List;
import java.util.Optional;

import com.conygre.spring.entities.Trade;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TradeRepository extends MongoRepository<Trade, ObjectId> {

    public List<Trade> findAll();

    public Optional<Trade> findById(ObjectId id);

	public List<Trade> findByTradeStatus(String tradeStatus);

    
}