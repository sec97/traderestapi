/*package com.conygre.spring.service;

import java.util.Collection;
import java.util.Optional;

import com.conygre.spring.entities.Trade;
import com.conygre.spring.repo.TradeRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TradeServiceImpl implements TradeService {

    @Autowired
    private TradeRepository dao;
    
    @Override
    public void addTrade(Trade t) {
        dao.insert(t);

    }

    @Override
    public Collection<Trade> getTrades() {
        return dao.findAll();
    }


    @Override
    public Optional<Trade> getTradeById(ObjectId id) {
        return dao.findById(id);
    }

    @Override
    public void deleteTrade(Trade t) {
        dao.delete(t);

    }
    
}*/