package com.conygre.spring.rest;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.List;

import com.conygre.spring.entities.Trade;
import com.conygre.spring.service.TradeService;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/trades")
public class TradeController {

    @Autowired
    private TradeService tradeService;

    @RequestMapping(method = RequestMethod.GET)
    public Collection<Trade> getTrades() {
        return tradeService.getTrades();
    }

    @RequestMapping(value="/{id}", method = RequestMethod.GET) 
    public Optional<Trade> getTradeById(@PathVariable("id") ObjectId id) {
        return tradeService.getTradeById(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public void addTrade(@RequestBody Trade t)
    {
        t.setTotalPrice(t.getStockPrice()*t.getStockQuantity());
        LocalDateTime timeStamp = LocalDateTime.now();
        t.setTimeStamp(timeStamp);
        t.setTradeStatus("CREATED");
        tradeService.addTrade(t);
    }

    @RequestMapping(value="/{id}", method = RequestMethod.DELETE)
    public void deleteTradeById(@PathVariable("id") ObjectId id){
        tradeService.deleteTradeById(id);
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.PUT)
    public void updateTrade(@PathVariable("id") ObjectId id, @RequestBody Trade t)
    {
        t.setId(id);
        t.setTotalPrice(t.getStockPrice()*t.getStockQuantity());
        tradeService.updateTradeById(t);
    }

    @RequestMapping(value="/status/{status}", method=RequestMethod.GET)
    public List<Trade> getTradeByStatus(@PathVariable("status") String status)
    {
        return tradeService.getTradeByStatus(status);
    }
}